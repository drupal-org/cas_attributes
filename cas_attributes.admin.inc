<?php

/**
 * @file
 * Provides settings pages for the CAS Attributes module.
 */

/**
 * Administrative settings form.
 */
function cas_attributes_admin_settings() {
  // Get current settings, and add default options.
  $cas_attributes = variable_get('cas_attributes', array());
  $cas_attributes += array('sync_every_login' => NULL, 'relations' => array('mail' => NULL, 'name' => NULL), 'roles' => array('manage' => array(), 'mapping' => ''));
  $relations = $cas_attributes['relations'];

  $form['cas_attributes'] = array(
    '#tree' => TRUE,
    '#type' => NULL,
  );

  $form['cas_attributes']['sync_every_login'] = array(
    '#type' => 'radios',
    '#title' => t('Fetch CAS Attributes'),
    '#default_value' => $cas_attributes['sync_every_login'],
    '#options' => array(
      0 => 'only when a CAS account is created (i.e., the first login of a CAS user).',
      1 => 'every time a CAS user logs in.'
    ),
    '#weight' => -10,
  );

  $form['cas_attributes']['relations'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account profile attribute mappings'),
  );

  $form['cas_attributes']['relations']['help'] = array(
    '#markup' => t('Token replacement strings used to populate each <a href="@url">user field</a>? Entries left blank will be ignored.', array('@url' => url('admin/user/profile'))),
  );

  // Provide fields for username and e-mail address.
  $form['cas_attributes']['relations']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => isset($relations['name']) ? $relations['name'] : '',
    '#size' => 50,
    '#description' => t('The account username.'),
  );
  $form['cas_attributes']['relations']['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail address'),
    '#default_value' => isset($relations['mail']) ? $relations['mail'] : '',
    '#size' => 50,
    '#description' => t('The account e-mail address.'),
  );

  // Provide fields for fields attached to the user bundle.
  foreach (_cas_attributes_get_profile_fields() as $fid => $instance) {
    $form['cas_attributes']['relations'][$fid] = array(
      '#type' => 'textfield',
      '#title' => check_plain($instance->title),
      '#default_value' => isset($relations[$fid]) ? $relations[$fid] : '',
      '#size' => 50,
      '#description' => t('The LDAP attribute containing the account field with name %field_name', array('%field_name' => $instance->name)),
    );
  }

  // Role Management
  $managed_roles = $cas_attributes['roles'];
  $form['cas_attributes']['roles'] = array(
    '#type' => 'fieldset',
    '#title' => 'Role Mapping',
    '#access' => user_access('administer permissions'),
  );

  $roles = user_roles(TRUE);
  unset($roles[DRUPAL_AUTHENTICATED_RID]);
  asort($roles);

  $form['cas_attributes']['roles']['manage'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles to manage'),
    '#description' => t('Which roles should be managed by these attributes.'),
    '#default_value' => $managed_roles['manage'],
    '#options' => $roles,
  );

  $form['cas_attributes']['roles']['mapping'] = array(
    '#type' => 'textarea',
    '#title' => t('Attributes'),
    '#description' => t('Specify the CAS attributes to use for roles, one per line. For example, <strong>department</strong> or <strong>affiliations</strong>. Do not use token syntax, as it will not be processed. .'),
    '#default_value' => $managed_roles['mapping'],
  );

  // Content Profile compatibility.
  if (module_exists('content_profile')) {
    $cas_attributes += array('content_profile' => array());
    $content_profile = $cas_attributes['content_profile'];

    $form['cas_attributes']['content_profile'] = array(
      '#type' => 'fieldset',
      '#title' => t('Content Profile attribute mappings'),
    );
    foreach (content_profile_get_types('types') as $type_name => $type) {
      $fields = content_types($type_name);
      $content_profile += array($type_name => array());
      $values = $content_profile[$type_name];
      foreach ($fields['fields'] as $field_name => $field) {
        if ($field['type'] == 'text') {
          $form['cas_attributes']['content_profile'][$type_name][$field_name] = array(
            '#type' => 'textfield',
            '#title' => $field['widget']['label'],
            '#default_value' => isset($values[$field_name]) ? $values[$field_name] : '',
            '#size' => 50,
            '#description' => t('The LDAP attribute containing the Content Profile field with name %field_name', array('%field_name' => $field_name)),
          );
        }
      }
    }
  }

  $form['token_tree'] = array(
    '#value' => theme('token_tree', array('cas'), FALSE),
  );

  return system_settings_form($form);
}

/**
 * Lists available CAS Attributes.
 */
function cas_attributes_list() {
  cas_phpcas_load();
  cas_phpcas_init();

  phpCAS::forceAuthentication();
  $attributes = phpCAS::getAttributes();

  $header = array('Token', 'Value');
  $rows = array();

  foreach ($attributes as $attribute => $value) {
    if (is_array($value)) {
      $value = $value[0];
    }
    $rows[] = array(
      t('[cas-attribute-@attribute]', array('@attribute' => drupal_strtolower($attribute))),
      check_plain($value),
    );
  }

  if (empty($rows)) {
    $rows[] = array(
      'data' => array(
        array(
          'data' => t('No CAS attributes were returned by the CAS server.'),
          'colspan' => 2,
        ),
      ),
    );
  }

  return theme('table', $header, $rows);
}
